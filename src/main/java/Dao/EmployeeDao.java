/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;
import model.Product;
//import model.Salary;

/**
 *
 * @author sany
 */
public class EmployeeDao implements DaoInterface<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        int id = -1;
        try {
            String sql = "INSERT INTO employee (name,salary,type,timeToStart,timeToEnd,date)\n"
                    + "VALUES (?,?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getSalary());
            stmt.setString(3, object.getType());
            stmt.setDouble(4, object.getTimeToStart());
            stmt.setDouble(5, object.getTimeToEnd());
            stmt.setString(6, object.getDate());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: cannot add Employee");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        try {
            String sql = "SELECT * FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                double salary = result.getDouble("salary");
                String type = result.getString("type");
                Double timeToStart = result.getDouble("timeToStart");
                Double timeToEnd = result.getDouble("timeToEnd");
                String date = result.getString("date");
                String confirm = result.getString("confirm");

                Employee employee = new Employee(id, name, salary, type, timeToStart, timeToEnd, date, confirm);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("Error cant getAll info employee");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        try {
            String sql = "SELECT * FROM employee WHERE id =" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                double salary = result.getDouble("salary");
                String type = result.getString("type");
                Double timeToStart = result.getDouble("timeToStart");
                Double timeToEnd = result.getDouble("timeToEnd");
                String date = result.getString("date");
                String confirm = result.getString("confirm");
                Employee employee = new Employee(id, name, salary, type, timeToStart, timeToEnd, date, confirm);
                return employee;
            }
        } catch (SQLException ex) {
            System.out.println("Error: get employee");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        int row = 0;
        try {
            String sql = "DELETE FROM employee WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error delete employee");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        int row = 0;
        try {
            String sql = "UPDATE employee SET name = ?,salary = ? , type = ? ,confirm = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getSalary());
            stmt.setString(3, object.getType());
            stmt.setString(4, object.getConfirm());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: update employee");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
        return row;
    }

    public ArrayList<Employee> getEmployee(String text) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, salary, type, timeToStart, timeToEnd, date, confirm "
                    + "FROM employee WHERE id || name || salary ||type || timeToStart || timeToEnd "
                    + "||date || confirm LIKE '%" + text + "%'";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                double salary = result.getDouble("salary");
                String type = result.getString("type");
                double timeToStart = result.getDouble("timeToStart");
                double timeToEnd = result.getDouble("timeToEnd");
                String date = result.getString("date");
                String confirm = result.getString("confirm");
                Employee employee = new Employee(id, name, salary, type, timeToStart, timeToEnd, date, confirm);
                list.add(employee);
                System.out.println(list);
            }
        } catch (SQLException ex) {
            System.out.println("Error: cannot SearchEmployeeSalary!!!");
        }
        db.close();
        return list;
    }

    /*
    public ArrayList<Employee> getSalary(String text) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, salary, type, timeToStart, timeToEnd, date, confirm "
                    + "FROM employee WHERE id || name || salary ||type || timeToStart || timeToEnd "
                    + "||date || confirm LIKE '%" + text + "%'";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                double salary = result.getDouble("salary");
                String type = result.getString("type");
                double timeToStart = result.getDouble("timeToStart");
                double timeToEnd = result.getDouble("timeToEnd");
                String date = result.getString("date");
                String confirm = result.getString("confirm");
                Employee employee = new Employee(id, name, salary, type, timeToStart, timeToEnd, date,confirm);
                list.add(employee);
                System.out.println(list);
            }
        } catch (SQLException ex) {
            System.out.println("Error: cannot SearchEmployeeSalary!!!");
        }
        db.close();
        return list;
    }
     */
    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
//        System.out.println(dao.getAll());
//        System.out.println(dao.get(5));
        // System.out.println(dao.add("Jubu", 10500.0, "full", 09.00, 18.00, "05/10/64"));

        System.out.println(dao.getEmployee("zai"));
        //System.out.println(dao.getSalary("zai"));
//        System.out.println(dao.getAll());

    }

}
