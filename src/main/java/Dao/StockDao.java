/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Stock;

/**
 *
 * @author User
 */
public class StockDao implements DaoInterface<Stock> {

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        int id = -1;
        try {
            String sql = "INSERT INTO stock (name, minimumAmount, remainingAmount, price) VALUES (?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getMinimumAmount());
            stmt.setInt(3, object.getRemainingAmount());
            stmt.setDouble(4, object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: cannot AddStock!!!");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, minimumAmount, remainingAmount, price FROM stock";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                int minimumAmount = result.getInt("minimumAmount");
                int remainingAmount = result.getInt("remainingAmount");
                double price = result.getDouble("price");
                Stock stock = new Stock(id, name, minimumAmount, remainingAmount, price);
                list.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println("Error: cannot getAllStock!!!");
        }
        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, minimumAmount, remainingAmount, price FROM stock WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int sid = result.getInt("id");
                String name = result.getString("name");
                int minimumAmount = result.getInt("minimumAmount");
                int remainingAmount = result.getInt("remainingAmount");
                double price = result.getDouble("price");
                Stock stock = new Stock(id, name, minimumAmount, remainingAmount, price);
                return stock;
            }
        } catch (SQLException ex) {
            System.out.println("Error: cannot getStock!!!");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        int row = 0;
        try {
            String sql = "DELETE FROM stock WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: cannot DeleteStock!!!");
        }

        db.close();
        return row;

    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE stock SET name = ?, minimumAmount = ?, remainingAmount = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getMinimumAmount());
            stmt.setInt(3, object.getRemainingAmount());
            stmt.setDouble(4, object.getPrice());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: cannot UpdateStock!!!");
        }

        db.close();
        return row;
    }

    public ArrayList<Stock> getStock(String text) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            //String sql = "SELECT * FROM stock WHERE id ORDER BY minimumAmount AND remainingAmount DESC='"+text+"'";
            //String sql = "SELECT * FROM stock WHERE id,name,minimumAmount,remainingAmount,price="+text+"";      
            String sql = "SELECT id, name, minimumAmount, remainingAmount, price "
                    + "FROM stock WHERE id || name || minimumAmount "
                    + "|| remainingAmount || price LIKE '%" + text + "%'";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                int minimumAmount = result.getInt("minimumAmount");
                int remainingAmount = result.getInt("remainingAmount");
                double price = result.getDouble("price");
                Stock stock = new Stock(id, name, minimumAmount, remainingAmount, price);
                list.add(stock);
                System.out.println(list);
            }
        } catch (SQLException ex) {
            System.out.println("Error: cannot SearchStock!!!");
        }
        db.close();
        return list;
    }

}
