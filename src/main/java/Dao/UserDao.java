/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Database.Database;
import model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sany
 */
public class UserDao {

    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        ////////////////////////////////////////////////////////////////////////
        int id = -1;
        try {
            String sql = "INSERT INTO user (name,tel,password) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPass());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error add");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
        return id;
    }

    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        try {
            String sql = "SELECT id,\n"
                    + "       name,\n"
                    + "       tel,\n"
                    + "       password\n"
                    + "  FROM user;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(id, name, tel, password);

                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error getAll");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
        return list;

    }

    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        try {
            String sql = "SELECT id,name,tel,password FROM user WHERE id =" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(pid, name, tel, password);
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("Error get");
        }
        return null;

        ////////////////////////////////////////////////////////////////////////
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        int row = 0;
        try {
            String sql = "DELETE FROM user WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error delete");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
        return row;
    }

    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        int row = 0;
        try {
            String sql = "UPDATE user SET name = ?,tel = ?, password = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPass());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error update");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
        return row;
    }

    public ArrayList<User> getUser(String text) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, tel, password"
                    + "FROM user WHERE id || name || tel"
                    + "|| password LIKE '%" + text + "%'";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(id, name, tel, password);
                list.add(user);
                System.out.println(list);
            }
        } catch (SQLException ex) {
            System.out.println("Error: cannot SearchUser!!!");
        }
        db.close();
        return list;
    }

    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
        System.out.println(dao.getUser("fong"));

    }

}
