/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author sany
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sany
 */
public class Database {

    private static Database instance = new Database();
    private Connection conn;

    private Database() {

    }

    public static Database getInstance() {
        String dbPath = "./db/CoffeeStore.db";
        try {
            if (instance.conn == null || instance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("data base connection");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found");
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
        }
        return instance;
    }

    public static void close() {
        try {
            if (instance.conn != null && !instance.conn.isClosed()) {
                instance.conn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.conn = null;
    }

    public Connection getConnection() {
        return instance.conn;

    }

}
