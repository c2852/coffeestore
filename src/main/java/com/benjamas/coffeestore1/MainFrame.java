/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benjamas.coffeestore1;

import model.User;
import Dao.UserDao;

/**
 *
 * @author sany
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        scrMain.setViewportView(new Login(this));
        hideButtonMenu();
    }

    public void hideButtonMenu() {
        txtHello.setVisible(false);
        lblShowNameLogin.setVisible(false);
        btnPointOfSell.setVisible(false);
        btnStockManagement.setVisible(false);
        btnCustomerManagement.setVisible(false);
        btnEmployeeManagement.setVisible(false);
        btnProductManagement.setVisible(false);
        btnSalayeOfEmployee.setVisible(false);
        btnUserManagement.setVisible(false);
        btnSummarySales.setVisible(false);
        btnLogout.setVisible(false);
    }

    public void showButtonMenu() {
        txtHello.setVisible(true);
        lblShowNameLogin.setVisible(true);
        btnPointOfSell.setVisible(true);
        btnStockManagement.setVisible(true);
        btnCustomerManagement.setVisible(true);
        btnEmployeeManagement.setVisible(true);
        btnProductManagement.setVisible(true);
        btnSalayeOfEmployee.setVisible(true);
        btnUserManagement.setVisible(true);
        btnSummarySales.setVisible(true);
        btnLogout.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrMain = new javax.swing.JScrollPane();
        scrMenu = new javax.swing.JScrollPane();
        pnlMenu = new javax.swing.JPanel();
        btnPointOfSell = new javax.swing.JButton();
        btnStockManagement = new javax.swing.JButton();
        btnCustomerManagement = new javax.swing.JButton();
        btnEmployeeManagement = new javax.swing.JButton();
        btnProductManagement = new javax.swing.JButton();
        btnSalayeOfEmployee = new javax.swing.JButton();
        btnSummarySales = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        txtHello = new javax.swing.JLabel();
        lblShowNameLogin = new javax.swing.JLabel();
        btnUserManagement = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        pnlMenu.setBackground(new java.awt.Color(255, 255, 255));

        btnPointOfSell.setBackground(new java.awt.Color(204, 255, 255));
        btnPointOfSell.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnPointOfSell.setText("Point of Sell");
        btnPointOfSell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPointOfSellActionPerformed(evt);
            }
        });

        btnStockManagement.setBackground(new java.awt.Color(204, 255, 255));
        btnStockManagement.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnStockManagement.setText("Stock Management");
        btnStockManagement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStockManagementActionPerformed(evt);
            }
        });

        btnCustomerManagement.setBackground(new java.awt.Color(204, 255, 255));
        btnCustomerManagement.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCustomerManagement.setText("Customer Management");
        btnCustomerManagement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerManagementActionPerformed(evt);
            }
        });

        btnEmployeeManagement.setBackground(new java.awt.Color(204, 255, 255));
        btnEmployeeManagement.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnEmployeeManagement.setText("Employee Management");
        btnEmployeeManagement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmployeeManagementActionPerformed(evt);
            }
        });

        btnProductManagement.setBackground(new java.awt.Color(204, 255, 255));
        btnProductManagement.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnProductManagement.setText("Product Management");
        btnProductManagement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductManagementActionPerformed(evt);
            }
        });

        btnSalayeOfEmployee.setBackground(new java.awt.Color(204, 255, 255));
        btnSalayeOfEmployee.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSalayeOfEmployee.setText("Salary of Employee");
        btnSalayeOfEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalayeOfEmployeeActionPerformed(evt);
            }
        });

        btnSummarySales.setBackground(new java.awt.Color(204, 255, 255));
        btnSummarySales.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSummarySales.setText("Summary Sales");
        btnSummarySales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSummarySalesActionPerformed(evt);
            }
        });

        btnLogout.setBackground(new java.awt.Color(255, 204, 204));
        btnLogout.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        txtHello.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtHello.setText("HELLO!");

        lblShowNameLogin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnUserManagement.setBackground(new java.awt.Color(204, 255, 255));
        btnUserManagement.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnUserManagement.setText("User Management");
        btnUserManagement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserManagementActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlMenuLayout = new javax.swing.GroupLayout(pnlMenu);
        pnlMenu.setLayout(pnlMenuLayout);
        pnlMenuLayout.setHorizontalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnPointOfSell, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnStockManagement, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCustomerManagement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEmployeeManagement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnProductManagement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSalayeOfEmployee, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSummarySales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnLogout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMenuLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtHello)
                        .addGap(18, 18, 18)
                        .addComponent(lblShowNameLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnUserManagement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlMenuLayout.setVerticalGroup(
            pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtHello, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblShowNameLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnPointOfSell, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnProductManagement, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnStockManagement, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCustomerManagement, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnEmployeeManagement, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSalayeOfEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnUserManagement, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSummarySales, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        scrMenu.setViewportView(pnlMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrMain, javax.swing.GroupLayout.DEFAULT_SIZE, 1027, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(scrMain)
                    .addComponent(scrMenu))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        scrMain.setViewportView(new Login(this));
        hideButtonMenu();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnPointOfSellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPointOfSellActionPerformed
        scrMain.setViewportView(new PointOfSell(this));
    }//GEN-LAST:event_btnPointOfSellActionPerformed

    private void btnStockManagementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStockManagementActionPerformed
        scrMain.setViewportView(new StockPanel());
    }//GEN-LAST:event_btnStockManagementActionPerformed

    private void btnCustomerManagementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomerManagementActionPerformed
        scrMain.setViewportView(new CustomerPanel());
    }//GEN-LAST:event_btnCustomerManagementActionPerformed

    private void btnEmployeeManagementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEmployeeManagementActionPerformed
        scrMain.setViewportView(new EmployeePanel());
    }//GEN-LAST:event_btnEmployeeManagementActionPerformed

    private void btnProductManagementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductManagementActionPerformed
        scrMain.setViewportView(new ProductManagementPanel());
    }//GEN-LAST:event_btnProductManagementActionPerformed

    private void btnSalayeOfEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalayeOfEmployeeActionPerformed
         scrMain.setViewportView(new SalaryPanel());
    }//GEN-LAST:event_btnSalayeOfEmployeeActionPerformed

    private void btnSummarySalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSummarySalesActionPerformed
        scrMain.setViewportView(new SummaryPanel());
    }//GEN-LAST:event_btnSummarySalesActionPerformed

    private void btnUserManagementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserManagementActionPerformed
        scrMain.setViewportView(new UserPanel());
    }//GEN-LAST:event_btnUserManagementActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCustomerManagement;
    private javax.swing.JButton btnEmployeeManagement;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnPointOfSell;
    private javax.swing.JButton btnProductManagement;
    private javax.swing.JButton btnSalayeOfEmployee;
    private javax.swing.JButton btnStockManagement;
    private javax.swing.JButton btnSummarySales;
    private javax.swing.JButton btnUserManagement;
    private javax.swing.JLabel lblShowNameLogin;
    private javax.swing.JPanel pnlMenu;
    private javax.swing.JScrollPane scrMain;
    private javax.swing.JScrollPane scrMenu;
    private javax.swing.JLabel txtHello;
    // End of variables declaration//GEN-END:variables

    public void swtichToPointOfSell() {
        scrMain.setViewportView(new PointOfSell(this));
        showButtonMenu();
        lblShowNameLogin.setText(User.getCurrenUser().getName());

    }
}
