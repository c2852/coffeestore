/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Dao.CustomerDao;
import java.util.ArrayList;

/**
 *
 * @author sany
 */
public class Customer {

    private int id;
    private String name;
    private String tel;
    private int point;
    private static ArrayList<Customer> customerList = new ArrayList<>();
    //private static Customer currentCustomer = null;
    
    public Customer(String name, String tel) {
        this.name = name;
        this.tel = tel;
    }

    
    public Customer(int id, String name, String tel) {
        this.id = id;
        this.name = name;
        this.tel = tel;
    }

    public Customer(int id, String name, String tel,int point) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.point = point ;
    }

    public Customer(String name, String tel, int point) {
        this(-1, name, tel, point);
    }

    public Customer(String tel, int point ) {
        this(-1, "", tel , point);
    }
    
    public Customer(int point ) {
        this(-1, "", "" , point);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    
    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
    

    public static Customer checkCustomer(String tel) {
        CustomerDao dao = new CustomerDao();
        ArrayList<Customer> list = dao.getAll();

        System.out.println(list);

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getTel().equals(tel)) {
                //currentCustomer = list.get(i);
                return list.get(i);
            }

        }
        return null;
    }

    @Override
    public String toString() {
        return "customer{" + "id = " + id + ", name = " + name + ", tel = " + tel + '}';
    }

  

}
