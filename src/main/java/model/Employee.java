/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Acer
 */
public class Employee {
    private int id ;
    private String name;
    private double salary;
    private String type;
    private double timeToStart;
    private double timeToEnd;
    private String date;
    private String confirm;

    public Employee(int id, String name, double salary, String type, double timeToStart, double timeToEnd, String date, String confirm) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.type = type;
        this.timeToStart = timeToStart;
        this.timeToEnd = timeToEnd;
        this.date = date;
        this.confirm = confirm;
    }
    public Employee(int id, String name, double salary, String type, double timeToStart, double timeToEnd, String date) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.type = type;
        this.timeToStart = timeToStart;
        this.timeToEnd = timeToEnd;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getTimeToStart() {
        return timeToStart;
    }

    public void setTimeToStart(double timeToStart) {
        this.timeToStart = timeToStart;
    }

    public double getTimeToEnd() {
        return timeToEnd;
    }

    public void setTimeToEnd(double timeToEnd) {
        this.timeToEnd = timeToEnd;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", salary=" + salary + ", type=" + type + ", timeToStart=" + timeToStart + ", timeToEnd=" + timeToEnd + ", date=" + date + ", confirm=" + confirm + '}';
    }

}
