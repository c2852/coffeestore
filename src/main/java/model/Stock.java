/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class Stock {
    private int id;
    private String name;
    private int minimumAmount;
    private int remainingAmount;
    private double price;

    public Stock(int id, String name, int minimumAmount, int remainingAmount, double price) {
        this.id = id;
        this.name = name;
        this.minimumAmount = minimumAmount;
        this.remainingAmount = remainingAmount;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(int minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public int getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(int remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", name=" + name + ", minimumAmount=" + minimumAmount + ", remainingAmount=" + remainingAmount + ", price=" + price + '}';
    }
    
    
}
