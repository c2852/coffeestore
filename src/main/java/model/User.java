/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Dao.UserDao;
import java.util.ArrayList;

/**
 *
 * @author sany
 */
/**
 *
 * @author sany
 */
public class User {

    private int id;
    private String name;
    private String tel;
    private String password;
    private static User currenUser = null;
    private static ArrayList<User> userList = new ArrayList<>();

      public User(int id, String name, String tel, String password) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.password = password;
    }

    public User(int id, String name, String tel) {
        this(id, name, tel, "");
    }

    public User(String name, String tel, String password) {
        this(-1, name, tel, password);
    }

    public User(String name, String password) {
        this(-1, name, "", password);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPass() {
        return password;
    }

    public void setPass(String pass) {
        this.password = pass;
    }


    public static User login(String userName, String password) {
        UserDao dao = new UserDao();
        ArrayList<User> list = dao.getAll();
        
        System.out.println(list);
        
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(userName) && list.get(i).getPass().equals(password)) {
                currenUser = list.get(i);
                return list.get(i);
            }

        }
        return null;
    }

    public static boolean isLogin() {
        return currenUser != null;
    }

    public static User getCurrenUser() {
        return currenUser;
    }

    public static void logout() {
        currenUser = null;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", pass=" + password + '}';
    }

}
