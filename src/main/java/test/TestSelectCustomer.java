/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import Database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Customer;


/**
 *
 * @author sany
 */
public class TestSelectCustomer {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        try {
            String sql = "SELECT id,\n"
                    + "       tel,\n"
                    + "       name\n" 
                    + "  FROM customer;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String tel = result.getString("tel");
                String name = result.getString("name");

                Customer customer = new Customer(id, tel,name);
                System.out.println(customer);
            }
        } catch (SQLException ex) {
            System.out.println("Error Select Customer");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
    }

}
