/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import Database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Product;

/**
 *
 * @author sany
 *
 *
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        ////////////////////////////////////////////////////////////////////////
        try {
            String sql = "SELECT id,\n"
                    + "       name,\n"
                    + "       price,\n"
                    + "       amount\n"
                    + "  FROM product;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                int amount = result.getInt("amount");
                Product product = new Product(id, name, price, amount);
                System.out.println(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error Select Product");
        }

        ////////////////////////////////////////////////////////////////////////
        db.close();
    }

}
