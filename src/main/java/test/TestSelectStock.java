/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import Database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Stock;

/**
 *
 * @author User
 */
public class TestSelectStock {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,\n"
                    + "       name,\n"
                    + "       minimumAmount,\n"
                    + "       remainingAmount,\n"
                    + "       price\n"
                    + "  FROM stock;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                int minimumAmount = result.getInt("minimumAmount");
                int remainingAmount = result.getInt("remainingAmount");
                double price = result.getDouble("price");

                Stock stock = new Stock(id, name, minimumAmount, remainingAmount, price);
                System.out.println(stock);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Select Stock!!!");
        }

        db.close();
    }

}
